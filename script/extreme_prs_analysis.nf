#!/usr/bin/env nextflow
nextflow.enable.dsl=2
params.version=false
params.help=false
version='0.0.3'

timestamp='2021-10-26'
if(params.version) {
    System.out.println("")
    System.out.println("Run extreme phenotype analysis - Version: $version ($timestamp)")
    exit 1
}

params.extreme=1
params.normal=5
params.perm=10000
if(params.help){
    System.out.println("")
    System.out.println("Run extreme phenotype analysis - Version: $version ($timestamp)")
    System.out.println("Usage: ")
    System.out.println("    nextflow run extreme_analysis.nf [options]")
    System.out.println("Mandatory arguments:")
    System.out.println("    --prsice          PRSice executable")
    Sysmte.out.println("    --parents         Parent information")
    Sysmte.out.println("    --siblings        Sibling information")
    System.out.println("    --geno            Genotype file prefix ")
    System.out.println("    --fam             QCed fam file ")
    System.out.println("    --snp             QCed SNP file ")
    System.out.println("    --redundent       Redundent trait to be removed")
    System.out.println("    --db              UKB Phenotype database")
    System.out.println("    --ldsc            Folder to LD score software")
    System.out.println("    --score           Folder containing LD Scores. Should have")
    System.out.println("                      format of baseline-chr and weight-chr")
    System.out.println("    --weight          Weight LD score prefix")
    System.out.println("    --fieldFinders    Folder containing the field finders.")
    System.out.println("                      Should have field_finder as suffix")
    System.out.println("    --drop            Samples who withdrawn consent")
    System.out.println("    --cov             Covariate file containing batch and PCs")
    System.out.println("    --showcase        Data showcase, use to select phenotypes")
    System.out.println("    --extreme         Top x percentile we considered as extreme. Default: ${params.extreme}")
    System.out.println("    --normal          x percentiles from top and bottom that we considered")
    System.out.println("                      as normal. Default: ${params.normal}")
    System.out.println("    --perm            Total permutation. Default: ${params.perm}")
    System.out.println("    --blood           Blood phenotypes from Will")
    System.out.println("    --label           Labeling for relationship plot")
    System.out.println("    --tade            Modified python script from tade")
    System.out.println("    --help            Display this help messages")
} 

// include our processes from other file 
include {   extract_quantitative_traits
            extract_phenotype_from_sql
            residualize_phenotype
            useless_extraction
            extract_fecundity_from_sql
            adjust_fecundity
            split_base_target
            get_blood_trait_name_from_will
            extract_blood_traits  } from './module/phenotype_extraction'

include {   run_gwas
	    run_freq
            munge_sumstat
            run_ldsc
            run_genetic_correlation
            combine_genetic_correlation
            run_prsice
            modify_prs    }   from './module/prs_analysis'
include {   gather_file as gather_herit
            gather_file as gather_prs_raw
            gather_file as gather_prs_inverse
            gather_file as gather_raw 
            gather_file as gather_plot }   from './module/misc'
include {   merge_information
            statistical_analysis
            statistical_analysis2
            dichotomize_extreme
            partial_method_correlation
            combine_correlation_results
            qq_plot
            heatmap
            reg_mean_quantile_plots
            sib_reg_quantile_plots
            prune_phenotypes
            prune_phenotypes_sib
            plot_relationship
            reg_mean_quantile_plots_output
            tade_analyses
            tade_annoying_prs
            organize_tade
            clive_prs   }   from './module/downstream'

include {   filter_snp
            make_geno
            get_allele_count } from './module/beatrice'

// helper functions
def fileExists(fn){
   if (fn.exists()){
       return fn;
   }else
       error("\n\n-----------------\nFile $fn does not exist\n\n---\n")
}
def get_prefix(str){
    last = str.split("/").last();
    if(last[-1] == "/" || last[-1] != "."){
        return "./";
    }else{
        return str.split("/").last();
    }
}
def get_prefix_ldsc(str){
    if(str.split('-')[0] == "NA"){
        error("\n\n----------\n$str cannot be tokenized\n")
    }else
        return str.split('-')[0];
}
// Defining variables 
redundent = Channel.fromPath("${params.redundent}")
showcase = Channel.fromPath("${params.showcase}")
label = []
if(params.label){
    label =  Channel.fromPath("${params.label}")
}
db = Channel.fromPath("${params.db}")
qcFam = Channel.fromPath("${params.fam}")
qcSNP = Channel.fromPath("${params.snp}")
siblings = Channel.fromPath("${params.siblings}")
cov = Channel.fromPath("${params.cov}")
withdrawn = Channel.fromPath("${params.drop}")
external_blood = Channel.fromPath("${params.blood}")
genotype = Channel
    .fromFilePairs("${params.geno}.{bed,bim,fam}",size:3, flat : true){ file -> file.baseName }
    .ifEmpty { error "No matching plink files" }        
    .map { a -> [fileExists(a[1]), fileExists(a[2]), fileExists(a[3])] } 
prsice = Channel.fromPath("${params.prsice}")
ldsc = Channel.fromPath("${params.ldsc}/ldsc.py")
munge = Channel.fromPath("${params.ldsc}/munge_sumstats.py")
score = Channel.fromPath("${params.scores}/*") \
    | map { a -> [ get_prefix_ldsc(a.baseName)+"-", file(a)]} \
    | groupTuple()
tade_python = Channel.fromPath("${params.tade}")
tade_prs = Channel.fromPath("${params.tadePRS}")
// The main workflow
workflow{
    // 1. First, we need to select phenotypes of interest
    phenotype_selection()
    // 2. Then we perform the PRS analysis.
    prs_analysis(phenotype_selection.out.phenotype)
    // 3. Perform the regression to the mean and sibling test analysis
    extreme_analysis(   
        phenotype_selection.out.phenotype, 
        phenotype_selection.out.fecundity,
        prs_analysis.out.prs,
        prs_analysis.out.summary
   )
}



// This workflow is responsible for selecting and transforming the phenotype
// used in this analysis
workflow phenotype_selection{
    main:
        // 1. Identify valid phenotypes (continuous + >10,000 samples)
        extract_quantitative_traits(showcase)
        // 2. Extract Fecundity and paternal age information from our database
        extract_fecundity_from_sql(db)
        // 3. Adjust fecundity and paternal age based on Sanjak et al (2018)
        adjust_fecundity(extract_fecundity_from_sql.out, withdrawn)
        // 4. Extract phenotypes from database
        nonblood = extract_quantitative_traits.out.nonblood
            .splitCsv(header: true) 
            .map{row -> ["NonBlood", "${row.FieldID}", "${row.Coding}"]} 
        blood = extract_quantitative_traits.out.blood
            .splitCsv(header: true) 
            .map{row -> ["Blood", "${row.FieldID}", "${row.Coding}"]} 
        nonblood \
            | mix(blood) \
            | combine(db) \
            | extract_phenotype_from_sql

        // 5. Remove outliers from data (6SD away from mean), then 
        //    either inverse normalize + residualize or just
        //    residualize (Age + Sex + Batch + Centre + 40 PC).
        //    For blood traits, we also include Dilusion factor and fasting time
        //    as covariate and remove samples who take statin
        // data_type= Channel.of("raw", "inverse")
        // speed up a bit here
        data_type= Channel.of("inverse")
        extract_phenotype_from_sql.out \
            | combine(withdrawn) \
            | combine(siblings) \
            | combine(qcFam) \
            | combine(cov) \
            | combine(data_type)\
            | (residualize_phenotype & useless_extraction)
        get_blood_trait_name_from_will(external_blood) \
            | splitCsv(header: true) \
            | map{ a -> [a.trait, a.type, a.normalize]} \
            | combine(external_blood) \
            | combine(withdrawn) \
            | combine(siblings) \
            | combine(qcFam) \
            | combine(cov) \
            | extract_blood_traits
        
        pheno_out = residualize_phenotype.out \
            | mix(extract_blood_traits.out)
    emit: 
        phenotype = pheno_out
        fecundity = adjust_fecundity.out
}


workflow prs_analysis{
    take: pheno
    main:
        // 1. split data into base and target (50/50). We will always put
        //    samples with siblings in the target to maximize sample size
        //    for downstream analyses
        pheno \
            | split_base_target
        // 2. peform GWAS on base
        split_base_target.out.base \
            | combine(genotype) \
            | combine(qcSNP) \
            /* These are phenotypes that we don't want to perform PRS on as they are use for the other test*/
            | filter{ a -> !(a[0] == 137 || a[0] == 135 || a[0] == 2405 || a[0] == 2734)} \
            | (run_gwas & run_freq )
        // 3. Extract the ld score to appropriate groups
        baseline = score \
            | filter { a -> (a[0] =~ /baseline/) }
        weight = score \
            | filter { a -> (a[0] =~ /weight/) }
        // 3. Perform LDSC on base GWAS to obtain the SNP heritability
        run_gwas.out \
            | combine(munge) \
            | munge_sumstat \
            | combine(ldsc) \
            | combine(baseline) \
            | combine(weight) \
            | run_ldsc
        
        // 4. Get trait heritability information
        heritRes = run_ldsc.out \
            | map{a -> a[3]} \
            | collect
        
        gather_herit(heritRes, "Herit.info")
        // 5. Only retain Traits with SNP based heritability > 5%
        valid_field = run_ldsc.out
            .filter{ it[4].toFloat() > 0.05 }
            .map{ it -> [it[0].toString(), it[1].toString(), it[2].toString()] }
        // 5a. Perform genetic correlation analyses
        valid_munge = munge_sumstat.out \
            | combine(valid_field, by: [0, 1 ,2])

        valid_munge \
            | combine(valid_munge) \
            | filter{ a -> ((a[2] == "inverse" || a[2] == "Will_Blood") && a[2] == a[6] && a[0].toFloat() < a[4].toFloat())} \
            | map{ it -> [it[2], it[0], it[3],it[7]]} \
            | groupTuple(by: [0, 1, 2]) \
            | combine(ldsc) \
            | combine(baseline) \
            | combine(weight) \
            | run_genetic_correlation \
            | map{ it -> [it[1], it[2]]} \
            | groupTuple(by: 0) \
            | combine(showcase) \
            | combine_genetic_correlation
        // 6. Perform PRS on trait with SNP h2 > 5%
        run_gwas.out \
            | combine(valid_field, by: [0, 1, 2]) \
            | combine(prsice) \
            | combine(genotype) \
            | combine(qcSNP) \
            | combine(split_base_target.out.target, by: [0, 1, 2]) \
            | run_prsice

        run_prsice.out.score \
            | combine(run_prsice.out.snps, by: [0,1,2]) \
            | filter_snp
        // filter_snp.out \
        //     | combine(run_prsice.out.score, by: [0,1,2]) \
        //     | combine(genotype) \
        //     | make_geno

    //   run_gwas.out \
    //        | combine(valid_field, by: [0, 1, 2]) \
    //        | combine(split_base_target.out.target, by: [0, 1, 2]) \
    //        | combine(make_geno.out, by: [0,1,2]) \
    //        | get_allele_count

        run_prsice.out.score \
            | modify_prs
        prsRes = modify_prs.out \
            | groupTuple
        raw = prsRes \
            | filter{ a -> a[0] == "raw"} \
            | map{ a -> a[1]}
        inverse = prsRes \
            | filter { a -> a[0] != "raw"} \
            | map{ a -> a[1]}
            
        inverseRes = gather_prs_inverse(inverse.collect(), "PRS-inverse.info") \
            | map{ a -> ["inverse", a]}
        prsRes = gather_prs_raw(raw.collect(), "PRS-normalized.info") \
            | map{ a -> ["raw", a]} \
            | mix(inverseRes)

    emit:
        prs = run_prsice.out.score
        summary = prsRes
}

workflow extreme_analysis{
    take: pheno
    take: fecundity
    take: prs
    take: summary
    main:
    //  1. Combine all information in preparation of downstream analyses
        pheno \
            | combine(prs, by: [0, 1, 2]) \
            | combine(fecundity) \
            | combine(Channel.of("${params.extreme}")) \
            | combine(Channel.of("${params.normal}")) \
            | merge_information
        pheno \
            | combine(prs, by: [0, 1, 2]) \
            | combine(fecundity) \
            | clive_prs
        merge_information.out.tade \
            | combine(tade_python) \
            | tade_analyses

        merge_information.out.tadePRS \
            | combine(tade_prs) \
            | tade_annoying_prs
        tade_h2 = tade_analyses.out.h2 \
            | combine(showcase) \
            // this restrict our results to those that passed PRS threshold
            |  combine(prs, by: [0, 1, 2]) \
            | map{ a -> [   a[2],   // normalize
                            "tade-h2",
                            a[4],   // showcase file
                            a[3]]   /* h2 file */}  \
            | groupTuple(by: [0, 1, 2]) 

         tade_data = tade_analyses.out.data \
            | combine(showcase) \
            // this restrict our results to those that passed PRS threshold
            |  combine(prs, by: [0, 1, 2]) \
            | map{ a -> [   a[2],   // normalize
                            "tade-data",
                            a[4],   // showcase file
                            a[3]]   /* data file */}  \
            | groupTuple(by: [0, 1, 2]) 
        tade_tail= tade_analyses.out.tail \
            | combine(showcase) \
            // this restrict our results to those that passed PRS threshold
            |  combine(prs, by: [0, 1, 2]) \
            | map{ a -> [   a[2],   // normalize
                            "tade-tail",
                            a[4],   // showcase file
                            a[3]]   /* tail file */}  \
            | groupTuple(by: [0, 1, 2]) 
        tade_h2 \
            | mix(tade_tail) \
            | mix(tade_data) \
            | organize_tade

    //  2. We can now use the input to run the full analysis with permutation
    //     ignore illness code, live birth and number of children fathered
    /*
        For now, as we are not sure if our permutation procedure is correct, we will try to avoid
        doing the permutation and use the observed results directly for the downstream analysis
    */
    totalPerm = Channel.of(0)
    merge_information.out.standard \
        | combine(cov) \
        | combine(showcase) \
        | combine(Channel.of("${params.extreme}")) \
        | combine(Channel.of("${params.normal}")) \
        | combine(totalPerm) \
        |( statistical_analysis & statistical_analysis2)
    //  2b. Extract samples fall outside of expectation
    merge_information.out.standard \
        | combine(showcase) \
        | combine(Channel.of("${params.extreme}")) \
        | combine(Channel.of("${params.normal}")) \
        | dichotomize_extreme
    //  3. Calculate the correlation between methods
    //     If we do the correlation in one go, it will take up crazy amount of memory.
    //     So try to split it up
    processSize = Channel.of(10)
    permJobs = totalPerm \
        | combine(processSize) \
        | map{ a -> def fragments = a[0]/a[1]
            def res = []
            for(def i = 0; i < fragments; ++i){ res << i }
            return res } \
        | flatten \
        | combine(processSize) \
        | map{ a -> return [a[0] * a[1]+1, (a[0] + 1) * a[1]]} \
        | mix(Channel.of([0, "Ori"]))
    
    corInput = statistical_analysis.out.raw \
        | groupTuple
    corInput2 = statistical_analysis2.out.raw \
        | groupTuple
    //  
    //  3. And we can also use previous output to generate all required plots

    //  4. Generate overall raw result file
    raw = statistical_analysis.out.raw \
        | map{ a -> [a[1]]} \
        | collect
    gather_raw(raw, "Raw-result.csv")   
    //  5. Generate the QQ plot
    gather_plot(raw, "Raw-res-for-plot.csv")
    //  2. Some phenotype might be highly correlated. To tackle this, we try to 
    //     calculate the phenotypic correlation, and generate a "prunned" set of 
    //     phenotype for downstream analyses that involves correlation calculation
    pruneInput = pheno \
        /* combine with PRS so that we ignore any phenotype that doesn't pass the LDSC filtering */
        | combine(prs, by: [0, 1, 2]) \
        | map{ a -> [   a[2],   // normalize
                        a[3]]   /* phenotype file */}  \
        | groupTuple(by: 0)
    summary \
        | combine(Channel.of(0.4)) /* correlation threhsold */ \
        | combine(showcase) \
        | combine(redundent) \
        | combine(pruneInput, by: 0)\
        | prune_phenotypes 
    // Try to do a prunning prioritize with sibling data rather than R2
    summary \
        | combine(Channel.of(0.4)) /* correlation threhsold */ \
        | combine(showcase) \
        | combine(redundent) \
        | combine(gather_raw.out) \
        | combine(pruneInput, by: 0)\
        | prune_phenotypes_sib
    prune_pheno = prune_phenotypes.out \
        | map{ a -> [   a[0],
                        a[1],
                        "PRS",
                        a[2]]}
    prune_sib = prune_phenotypes_sib.out \
        | map{ a -> [   a[0],
                        a[1],
                        "Sib",
                        a[2]]}
    prune_pheno \
        | mix(prune_sib)\
        | filter{ a -> a[0] != "raw"} \
        | combine(showcase) \
        | combine(Channel.of("t.stat", "p")) \
        | combine(corInput2, by: 0)  \
        | combine(label) \
        | plot_relationship
        
     prune_pheno \
        | mix(prune_sib)\
        | filter{ a -> a[0] != "raw"} \
        | combine(permJobs) \
        | combine(Channel.of("t.stat", "p")) \
        | combine(corInput2, by: 0) \
        | partial_method_correlation \
        | mix \
        | groupTuple(by: [0, 1, 2, 3, 4]) \
        | combine_correlation_results \
        | heatmap
   

    qq_plot(gather_plot.out)
    
    // Do the expectation plot for traits we want
    // output what Tade want for plotting
    merge_information.out.standard \
        | combine(showcase) \
        | reg_mean_quantile_plots_output
    merge_information.out.standard \
        | filter{ a -> a[2] == "inverse"} \
        | filter{ a -> a[0] == "30790" || a[0] == "30050" || a[0] == "20015" || a[0] == "30870" || a[0] == "30890" || a[0] == "51" || a[0] == "30710" || a[0] == "30750" || a[0] == "30730" || a[0] == "3148" || a[0] == "30190" || a[0] == "3786" || a[0] == "20127" || a[0] == "23099" || a[0] == "30050"	|| a[0] == "20015"	|| a[0] == "30090"	|| a[0] == "30020"	|| a[0] == "30150"	|| a[0] == "30260"	|| a[0] == "78"	|| a[0] == "30270"	|| a[0] == "23124"	|| a[0] == "30130"	|| a[0] == "30030"	|| a[0] == "30110" } \
        | combine(showcase) \
        | (reg_mean_quantile_plots & sib_reg_quantile_plots)
}








