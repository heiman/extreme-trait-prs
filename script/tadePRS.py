#!/usr/bin/env python
import sys
from collections import defaultdict as dd
from collections import Counter as cc
import numpy as np
from scipy import stats
import random 
import math 
from math import log
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import patches
from matplotlib import text as mtext
from scipy.stats import rankdata
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
import itertools
from statsmodels.stats.outliers_influence import variance_inflation_factor

from patsy import dmatrices

matplotlib.rcParams['xtick.major.pad'] = 2                                                                                                
matplotlib.rcParams['ytick.major.pad'] = 2                                                                                                
matplotlib.rcParams['axes.labelpad'] = 4                                                                                                    
#matplotlib.rcParams['xtick.direction'] = 'inout'                                                                                            
#matplotlib.rcParams['ytick.direction'] = 'inout'                                                                                           
#matplotlib.rcParams['ytick.direction'] = 'in'                                                                                               
matplotlib.rcParams['xtick.labelsize'] = 20                                                                                         
matplotlib.rcParams['ytick.labelsize'] = 20                                                                                       
matplotlib.rcParams['axes.linewidth'] = 1.1   


def myround(x, base=10):
    return int(base * round(float(x)/base))


def my_error(msg): 
    sys.stderr.write(msg+'\n') 
    sys.exit() 

plt.rc('text', usetex=True)
    

Z_PERCS = {} 
for k in [100,500,1000]: Z_PERCS[k] = [stats.norm.ppf(x/float(k)) for x in range(1,k)] + [100000] 

#Z_PERCS[25] = [-2.25, -2, -1.75, -1.5, -1.125, -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 2, 2.25, 100]
#Z_PERCS[50] = [-2, -1.5, -1, -0.5, 0, 0.5, 1, 1.5, 2, 100]


#Z_PERCS[100] = [stats.norm.ppf(x/100.0)  for x in range(1,100)] + [1000000000]  
#Z_PERCS[500] = [stats.norm.ppf(x/500.0)  for x in range(1,500)] + [1000000000]  
#Z_PERCS[1000] = [stats.norm.ppf(x/1000.0)  for x in range(1,1000)] + [1000000000]  
#Z_PERCS[2000] = [stats.norm.ppf(x/2000.0)  for x in range(1,2000)] + [1000000000]  
















































    
            
            
class RegTests:   
    def __init__(self, pt_data): 
        
        self.pD = pt_data
        step_data = pt_data.S[100] 
        self.Xp, self.Yp = step_data.Xp, step_data.Yp 
        self.result, self.result2 = {}, {}  
        
        
    def get_flat(self, a, b, TAIL = 'UPPER'):  
        X, Y, B = [], [], [] 
        if TAIL == 'UPPER': 
            for k in range(a,b): 
                X.extend(self.Xp[k])   
                Y.extend(self.Yp[k])   
                B.extend([0 for x in self.Xp[k]]) 
            X.extend(self.Xp[b])
            Y.extend(self.Yp[b]) 
            B.extend([1 for x in self.Xp[b]]) 
        if TAIL == 'LOWER': 
            for k in range(a,b+1): 
                X.extend(self.Xp[k])   
                Y.extend(self.Yp[k])   
                if k == a: B.extend([1 for x in self.Xp[k]]) 
                else:      B.extend([0 for x in self.Xp[k]]) 
        return X, Y, B 


        
    def go(self): 
        ms, ms2, ms3  = 'Y~X+B' , 'Y~X', 'Y~B' 
        for k in range(0,50): 
            X, Y, B = self.get_flat(k, k+25, TAIL = 'LOWER') 
            DF = {'Y': Y, 'X': X, 'B': B} 
            results = smf.ols(ms, data = pd.DataFrame(DF)).fit(disp=0) 
            params =   [results.params[j] for j in range(len(results.params))] 
            pvalues =  [results.pvalues[j] for j in range(len(results.pvalues))]
            self.result[k] = [params[-1], pvalues[-1]] 
            

            DF = {'Y': Y, 'X': X} 
            res2 = smf.ols(ms2, data = pd.DataFrame(DF)).fit()  
            y_resid = [r for r in res2.resid]  
            DF = {'Y': y_resid, 'B': B} 
            res3 = smf.ols(ms3, data = pd.DataFrame(DF)).fit(disp=0) 
            params =   [res3.params[j] for j in range(len(res3.params))] 
            pvalues =  [res3.pvalues[j] for j in range(len(res3.pvalues))]
            self.result2[k] = [params[-1], pvalues[-1]] 
        
        for k in range(50,100): 
            X, Y, B = self.get_flat(k-25, k, TAIL = 'UPPER') 
            DF = {'Y': Y, 'X': X, 'B': B} 
            results = smf.ols(ms, data = pd.DataFrame(DF)).fit(disp=0) 
            params =   [results.params[j] for j in range(len(results.params))] 
            pvalues =  [results.pvalues[j] for j in range(len(results.pvalues))] 
            self.result[k] = [params[-1], pvalues[-1]] 
        
            DF = {'Y': Y, 'X': X} 
            res2 = smf.ols(ms2, data = pd.DataFrame(DF)).fit()   
            y_resid = [r for r in res2.resid]  
            DF = {'Y': y_resid, 'B': B} 
            res3 = smf.ols(ms3, data = pd.DataFrame(DF)).fit(disp=0) 
            params =   [res3.params[j] for j in range(len(res3.params))] 
            pvalues =  [res3.pvalues[j] for j in range(len(res3.pvalues))]
            self.result2[k] = [params[-1], pvalues[-1]] 
        
        return self.result, self.result2 
            














class RegModel:
    def __init__(self, pt_data): 
        self.pD = pt_data
        
        step_data = pt_data.S[100] 
        self.Xp, self.Yp = step_data.Xp, step_data.Yp 
        
        self.xname, self.yname, self.X, self.Y = pt_data.xname, pt_data.yname, pt_data.X, pt_data.Y
        
         

    def runModel(self, FULL = False): 

        if FULL: 
            fdf, fstr = pd.DataFrame({self.yname: self.Y, self.xname: self.X}), self.yname +'~'+self.xname 
            model = smf.ols(fstr, data = fdf) 
            result = model.fit(disp=0) 
            return result , result.resid
        else: 
            bodyK = range(10,90) 
            Xb, Yb = [], [] 
            for k in bodyK: 
                Xb.extend(self.Xp[k]) 
                Yb.extend(self.Yp[k]) 
            model = smf.ols('Y~X', data = pd.DataFrame({'Y': Yb, 'X': Xb})) 
            result = model.fit(disp=0) 
            my_resids, my_params = [] , [p for p in result.params]
            for j,y in enumerate(self.Y): my_resids.append(y  - (my_params[0] + my_params[1]*self.X[j]))
            return result, my_resids     


    def getResidualResults(self): 
        full_model, full_resids = self.runModel(FULL=True) 
        body_model, body_resids = self.runModel(FULL=False) 
        self.fullResult, self.fullResids = self.getRes(full_resids) 
        self.bodyResult, self.bodyResids = self.getRes(body_resids) 
        return self 

    def getRes(self, resids): 
        rX = sorted(zip(self.X, resids)) 
        k,i, STAY, RV, RD = 0, 0, True, {}, {}

        if self.xname == 'prs' or self.pD.type == 'rank':
            locs = [ int( z * (len(resids) / 100.0) ) for z in range(1,101) ] 
            while True:  
                rV = [] 
                while i < len(rX) and i < locs[k]: 
                    rV.append(rX[i][1]) 
                    i+=1 
                RV[k] = rV 
                if i < len(rX): k+=1 
                else:           break 
        else:
            while True: 
                rV = []
                while i < len(rX) and rX[i][0] < Z_PERCS[100][k]: 
                    rV.append(rX[i][1]) 
                    i += 1 
                RV[k] = rV
                if i < len(rX): k+=1 
                else:           break 
        for k in sorted(RV.keys()): 
            t,pv = stats.ttest_1samp(RV[k], popmean = 0) 
            RD[k] = [np.mean(RV[k]), t, pv] 
        return RD, RV 

        
















class StepData:
    def __init__(self, sLoc, dType, xname, yname, locs, Xp, Yp): 
        
        self.sLoc = sLoc 
        self.type, self.xname, self.yname, self.locs, self.Xp, self.Yp = dType, xname, yname, locs, Xp, Yp 
        self.len, self.keys = len(Xp), sorted(self.Xp.keys()) 
        self.Xm, self.Ym = [np.mean(self.Xp[k]) for k in self.keys], [np.mean(self.Yp[k]) for k in self.keys]
        
    
        
        
            


class PtData:
    def __init__(self, pts, pX = 'xname', pY = 'yname', TYPE = 'vals', STEPSIZE=400): 
        self.xname, self.yname, self.len, self.pts, self.type = pX, pY, len(pts), sorted(pts), TYPE  
       
        if self.type.upper() == 'RANK': 
            pr = sorted([[self.pts[i][1], i] for i in range(len(self.pts))])
            self.pts  = sorted([[pr[i][1],i] for i in range(len(pr))]) 

        self.X, self.Y = [p[0] for p in self.pts], [p[1] for p in self.pts] 
        #self.get_limits() 
        self.get_step_data(STEPSIZE) 

    
    
    def get_step_data(self, steps): 
        self.X_sm, self.Y_sm, self.steplen = [], [], int(self.len / steps) 
        
        for i in range(0,len(self.pts),self.steplen): 
            self.X_sm.append(np.mean(self.X[i:i+self.steplen]))
            self.Y_sm.append(np.mean(self.Y[i:i+self.steplen]))
        
        self.S = {} 
        self.xRange  =  self.X[-1] - self.X[0] 
        
        locs = [100] 
        for loc in locs: 
            
            if self.type == 'rank':     self.S[loc] = self.get_ptile(loc, [ int( z * (self.len / float(loc))) for z in range(1,loc) ] + [self.len]) 
            elif self.xname == 'trait': self.S[loc] = self.get_ptile(loc, Z_PERCS[loc]) 
            else:                       self.S[loc] = self.get_ptile(loc, [self.X[0] + ((self.xRange / float(loc))*i) for i in range(1,loc)] + [self.X[-1]+1])
        return 
        
            
    
    
    
    
    
    def get_ptile(self, loc, locs): 
        Xp, Yp,i,k = {}, {} , 0, 0 
        while True: 
            X,Y = [], [] 
            while i < len(self.pts) and self.pts[i][0] < locs[k]: 
                X.append(self.pts[i][0]) 
                Y.append(self.pts[i][1]) 
                i+=1 
            Xp[k] = X 
            Yp[k] = Y 
            k+=1 
            if i == len(self.pts): break  
        return StepData(loc, self.type, self.xname, self.yname, locs, Xp, Yp) 



    
    def get_limits(self): 
        self.yMin, self.yMax = min(self.Y), max(self.Y) 
        self.yRange = self.yMax - self.yMin 
        self.yStep = self.yRange / 30.0 
        self.plotBottom = self.yMin - self.yStep 
        self.plotTop = self.yMax + self.yStep 
        self.xMin, self.xMax = self.X[0], self.X[-1]  
        self.xRange = self.xMax - self.xMin 
        self.xStep = self.xRange / 30.0 
        self.plotLeft = self.xMin - self.xStep 
        self.plotRight = self.xMax + self.xStep 
        

    
    def getRegWindows(self): 
        self.regTests = RegTests(self).go() 
        return 

    def getResTests(self):         
        self.resTests = RegModel(self).getResidualResults() 
        return 




    
            
            
            


    
        



    

    


def run_script(my_args,options):  
    
    name = my_args[0].split('/')[-1].split('.')[0] 
    f = open(my_args[0]) 
    pointPairs = [] 
    for i,line in enumerate(f): 
        lp = line.split() 
        lc = line.split(',') 
        
        if len(lp) == 4:    line_id, line_rule, prs, pheno = lp    
        elif len(lc) == 4:  line_id, line_rule, prs, pheno = lc
        else:               my_error('improper file') 

        if line_rule == 'Yes': pointPairs.append([float(pheno), float(prs)])  
    
    f.close() 

    

    w = sys.stdout 
    header = ['pt','datatype','win1_bw','win1_pv','win2_bw','win2_pv','res1_mean','res1_t','res1_pv','res2_mean','res2_t','res2_pv'] 
    w.write(','.join(header)+'\n')

        
    

    for di, datatype in enumerate(['vals','rank']): 

        ptData = PtData(pointPairs, pX = 'trait', pY='prs', TYPE = datatype) 
        ptData.getRegWindows() 
        ptData.getResTests() 
        win1, win2 = ptData.regTests 
        res1, res2 = ptData.resTests.fullResult, ptData.resTests.bodyResult


        for k in range(100): 
            win1_bw, win1_pv = win1[k] 
            win2_bw, win2_pv = win2[k] 
            res1_mean, res1_T, res1_pv = res1[k] 
            res2_mean, res2_T, res2_pv = res2[k] 
            
            
            my_data = [k,datatype,win1_bw, win1_pv, win2_bw, win2_pv, res1_mean, res1_T, res1_pv, res2_mean, res2_T, res2_pv] 
            w.write(','.join([str(xx) for xx in my_data])+'\n')     


    
    sys.exit() 
    
    
    




if __name__ == '__main__':
    from optparse import OptionParser
    usage = "usage: ./%prog [options] data_file"
    parser = OptionParser(usage=usage)
    
    
    
    parser.add_option("--tails", default = 'NA', type='string', help="Output Filename Prefix")
    parser.add_option("--evopts", default = 'NA', type='string', help="Output Filename Prefix")
    parser.add_option("--prspts", default = 'NA', type='string', help="Output Filename Prefix")
    parser.add_option("--sibtails", default = 'NA', type='string', help="Output Filename Prefix")
    parser.add_option("--sibpts", default = 'NA', type='string', help="Output Filename Prefix")
    parser.add_option("--choice", default = 'all', type='string', help="Output Filename Prefix")
    (options, args) = parser.parse_args()
    run_script(args,options)

        

        


