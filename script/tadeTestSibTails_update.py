#!/usr/bin/env python
import sys
from collections import defaultdict as dd
import numpy as np
from scipy import stats
import random
import math
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
plt.rc('text', usetex=True)
mpl.use("Cairo")

#############################################################################################
##################################  TRAIT PLOT     ##########################################
#############################################################################################


class TraitPlot:
    def __init__(self, name, k):
        self.name, self.k, self.figname, self.WD, self.HT = name, k, name+'.'+k+'.png', 12, 8

        self.fig = mpl.pyplot.gcf()
        self.fig.set_size_inches(self.WD, self.HT)
        self.setup()
        self.ax = plt.subplot2grid((1, 1), (0, 0), rowspan=1, colspan=1)

    def setup(self):
        self.color_rainbow = plt.get_cmap('coolwarm')
        self.herits = [round(i * 0.05, 2) for i in range(21)]
        self.h_colors = [self.color_rainbow(
            1.*i/float(len(self.herits))) for i in range(len(self.herits))]
        self.color_key = {h: c for h, c in zip(self.herits, self.h_colors)}

    def draw_summary_table(self, pairs, h2, tests):
        self.bH, s1, s2, s_len = round(h2['Bod'][1], 2), [], [], []
        X = sorted(pairs.keys())
        for x in X:
            s1.append(np.mean([p[0] for p in pairs[x]]))
            s2.append(np.mean([p[1] for p in pairs[x]]))
            s_len.append(len(pairs[x]))

        for i, h in enumerate(self.herits):
            sh = [(s * h)/2.0 for s in s1]
            if i == 0:
                self.ax.plot(
                    X, sh, color=self.color_key[h], linewidth=3, alpha=0.7)
            else:
                z1 = [(a+b+b)/3.0 for a, b in zip(sh, lp)]
                z2 = [(a+b)/2.0 for a, b in zip(sh, lp)]
                self.ax.plot(
                    X, z1, color=self.color_key[h], linewidth=4, alpha=0.3)
                self.ax.plot(
                    X, z2, color=self.color_key[h], linewidth=4, alpha=0.3)
                self.ax.plot(
                    X, sh, color=self.color_key[h], linewidth=4, alpha=0.3)
            lp = [z for z in sh]

        if round(max(abs(lp[0]), lp[-1])+0.05, 1) < 1.5:
            yMax = 1.5
        elif round(max(abs(lp[0]), lp[-1])+0.05, 1) < 1.8:
            yMax = 1.8
        else:
            yMax = 2

        if self.name != 'NA':
            self.ax.text(0, yMax*0.96, self.name, ha='left',
                         va='top', fontsize=30, fontweight='bold')

        sE = [(s*self.bH)/2.0 for s in s1]
        X[0], X[-1] = 0.5, 98.5
        sE[0] = (sE[0]+sE[0]+sE[1])/3.0
        sE[-1] = (sE[-2]+sE[-1]+sE[-1])/3.0
        self.ax.plot(X, sE, color='k', linewidth=2, alpha=1.0, zorder=2)

        for i, (x, y) in enumerate(zip(X, s2)):
            if x > 1 and x < 99:
                self.ax.scatter(x, (sE[i]+y+y+y)/4.0, s=25, alpha=0.75,
                                marker='o', color='grey', edgecolor='k', zorder=50)

        n0, m0 = tests['0']['NOVO'], tests['0']['MEND']
        n1, m1 = tests['99']['NOVO'], tests['99']['MEND']
        cL, cR, tL, tR = 'grey', 'grey', 'whitesmoke', 'whitesmoke'
        if n0.pv < 0.05:
            cL = 'lime'
        if n1.pv < 0.05:
            cR = 'lime'
        if m0.pv < 0.05:
            rL = 'red'
        if m1.pv < 0.05:
            tR = 'red'
        self.ax.scatter(0, n0.exp, edgecolor='k', marker='v', zorder=100,
                        facecolor=tL, linewidth=2.5, s=200, clip_on=False)
        self.ax.scatter(99, n1.exp, edgecolor='k', marker='^', zorder=100,
                        facecolor=tR, linewidth=2.5, s=200, clip_on=False)
        self.ax.scatter(0, n0.obs, edgecolor='k', facecolor=cL,
                        marker='h', zorder=100, s=200)
        self.ax.scatter(99, n1.obs, edgecolor='k', facecolor=cR,
                        marker='h', zorder=100, s=200)
        self.reset_fig(yMax)
        return

    def reset_fig(self, yMax):
        self.ax.set_yticks([-2, -1, 0, 1, 2])
        self.ax.set_xlim(-1.5, 100.5)
        self.ax.set_ylim(-yMax, yMax)
        self.ax.set_xlabel('Index Sibling Rank \% ($s_{(1)}$)', fontsize=22)
        self.ax.set_ylabel('Conditional Sibling Z-Value ($s_2$)', fontsize=22)
        plt.subplots_adjust(left=0.08, bottom=0.09, right=0.98,
                            top=0.99, wspace=0.14, hspace=0.46)
        plt.savefig(self.figname, dpi=300)
        plt.clf()
        self.fig = mpl.pyplot.gcf()
        self.fig.set_size_inches(self.WD, self.HT)
        self.ax = plt.subplot2grid((1, 1), (0, 0), rowspan=1, colspan=1)


#############################################################################################
##################################  FAMILY DATA  ############################################
#############################################################################################


class FamData:
    def __init__(self, filename):
        self.fams, self.members = [], []
        self.read_file(filename)
        self.collate()

    def read_file(self, filename):
        f = open(filename)
        for i, line in enumerate(f):
            line = line.split(",")
            try:
                k1, k2 = Kid(float(line[0]), str(float(i))), Kid(float(line[1]), str(i+0.5))
            except ValueError:
                if i == 0:
                    continue
                else:
                    raise Exception('Incorrect File Format')

            self.fams.append([k1, k2])
            self.members.extend([k1, k2])
        self.total = len(self.members)
        self.size = len(self.fams)
        f.close()
        return

    def collate(self):
        self.members.sort(key=lambda X: X.val)
        for i, k in enumerate(self.members):
            k.z = stats.norm.ppf((i+0.5)/self.total)
            k.rank = int(100*(i+0.5) / self.total)

        self.pairs = dd(lambda: dd(list))
        for k1, k2 in self.fams:
            kids = [k1, k2]
            random.shuffle(kids)
            rank = kids[0].rank
            self.pairs['vals'][kids[0].rank].append([kids[0].val, kids[1].val])
            self.pairs['norm'][kids[0].rank].append([kids[0].z, kids[1].z])
        return


class Kid:
    def __init__(self, val, idx):
        self.val, self.idx = val, idx
        self.z, self.rank = 'NA', 'NA'

    def __str__(self):
        return 'Individual-'+self.idx

    def __repr__(self):
        return 'Individual-'+self.idx


#############################################################################################
################################## TAIL TESTING   ###########################################
#############################################################################################


class TailTests:
    def __init__(self, name, pairs, h2):
        self.name, self.pairs, self.h2 = name, pairs, h2

    def get_data(self, pt, locs=[], ALL=False):
        my_data = []
        for loc in locs:
            my_data.extend(self.pairs[loc])
        if len(my_data) > 1:
            return my_data
        p = 1
        while len(my_data) < 2:
            if pt == 0:
                my_data.extend(self.pairs[locs[-1]+p])
            else:
                my_data.extend(self.pairs[locs[0]-p])
            p += 1
        return my_data

    def calculate(self):
        lower_tail = [[0],   [0, 1], [0, 1, 2],  [
            0, 1, 3],  [0, 1, 2, 3, 4], [0, 1, 2, 3, 4, 5]]
        upper_tail = [[99], [98, 99], [97, 98, 99], [96, 97, 98, 99], [
            95, 96, 97, 98, 99], [94, 95, 96, 97, 98, 99]]
        tail_locs = [0 for lt in lower_tail] + [1 for ut in upper_tail]

        bodyH = self.h2['Bod'][1]
        my_var = 1 - (bodyH*bodyH)/4.0
        my_std = my_var ** 0.5
        RES = dd(lambda: {})

        for vals, loc in zip(lower_tail+upper_tail, tail_locs):

            vs = str(vals[0])
            if vals[-1] != vals[0]:
                vs += '-'+str(vals[-1])
            my_data = self.get_data(loc, vals)
            
            index_sibs = sorted([md[0] for md in my_data]) 
            
            if loc == 0: index_sib = index_sibs[-1] 
            else:        index_sib = index_sibs[0] 
            
            
            RES[vs]['NOVO'] = TailTest(loc,index_sib).run_novo(my_data, bodyH, my_var, my_std)
            RES[vs]['MEND'] = TailTest(loc,index_sib).run_mend(my_data, bodyH, my_var, my_std)
            RES[vs]['DIST'] = TailTest(loc,index_sib).run_dist(my_data, bodyH, my_var, my_std)

        return RES





class TailTest:
    def __init__(self, loc, index_sib):
        if loc == 0: self.side = 'lower'
        else:        self.side = 'upper'
        self.index_sib = round(index_sib,5) 
        self.n_rate, self.m_rate, self.p_rate = 'NA', 'NA', 'NA' 



    def run_dist(self, tail_data, h2, h_var, h_std): 
        self.size = str(len(tail_data))
        S1, S2 = [s[0] for s in tail_data], [s[1] for s in tail_data] 
        expected = [(s1*0.5*h2) for s1,s2 in tail_data] 
        null_standardized = [(s2 - (s1*0.5*h2))/h_std for s1, s2 in tail_data] 
        self.Z, self.pv = stats.kstest(null_standardized, "norm") 
        self.exp = round(np.mean(expected),4) 
        self.obs = np.mean(S2) 
        tM = TailMax(self.side, tail_data, h2, h_var, h_std) 
        self.rates = tM.brute_force() 
        self.n_rate, self.m_rate, self.p_rate = round(self.rates[0],2), round(self.rates[1],2), round(self.rates[2],2) 
        return self 

    def run_novo(self, tail_data, h2, h_var, h_std):
        self.size = str(len(tail_data))
        SS = sum([s2 - (s1 * 0.5 * h2) for s1, s2 in tail_data])
        self.exp = round(np.mean([(s1*0.5*h2) for s1, s2 in tail_data]),4) 
        self.obs = np.mean([s2 for s1, s2 in tail_data])
        self.U = SS / h_var
        self.I = (-1*len(tail_data)) / h_var
        self.Z = SS / (len(tail_data)*h_var)**0.5
        if self.side != 'lower':
            self.pv = stats.norm.cdf(self.Z)
        else:
            self.pv = 1 - stats.norm.cdf(self.Z)
        return self

    def run_mend(self, tail_data, h2, h_var, h_std):
        self.size = str(len(tail_data))
        n = float(len(tail_data))
        idxMin, idxMax = tail_data[0][0], tail_data[-1][0]
        if self.side == 'lower':
            concord_probs = [stats.norm.cdf(
                idxMax, (s1*0.5*h2), h_std) for s1, s2 in tail_data]
            r = sum([s2 <= idxMax for s1, s2 in tail_data])
        else:
            concord_probs = [
                1-stats.norm.cdf(idxMin, (s1*0.5*h2), h_std) for s1, s2 in tail_data]
            r = sum([s2 >= idxMin for s1, s2 in tail_data])
        pi = np.mean(concord_probs)
        self.U = (r - n*pi) / (pi*(1-pi))
        self.I = -n / (pi*(1-pi))
        self.Z = (r - n*pi) / (n*pi*(1-pi))**0.5
        self.exp = round(n*pi,4) 
        self.obs = r
        self.pv = 1 - stats.norm.cdf(self.Z)
        if self.pv < 0.01 and self.obs - self.exp < 2:
            self.pv = 0.1 + random.random()/2.0
        return self



class TailMax: 
    def __init__(self, side, tail_data, h2, h_var, h_std, mend_std = 0.1): 
        self.side = side 
        self.tail_data, self.h2, self.h_var, self.h_std = tail_data, h2, h_var, h_std 
        self.cnts, self.probs = [0.0,0.0,0.0], [] 
        for s1,s2 in tail_data: 
            probs = [stats.norm.pdf(s2, 0, 1), stats.norm.pdf(s2, s1, mend_std), stats.norm.pdf(s2, (s1*0.5*h2), h_std)]
            self.probs.append(probs) 
            if probs[0] > probs[1] and probs[0] > probs[2]: self.cnts[0] += 1.0 
            elif probs[1] > probs[0]:                       self.cnts[1] += 1.0 
            else:                                           self.cnts[2] += 1.0 
        self.rates = [round(c/sum(self.cnts),2) for c in self.cnts] 
        
    def brute_force(self):  
        AD, P = [], [i/100.0 for i in range(100)] 
        for i,nv in enumerate(P): 
            ld = [] 
            for j,mv in enumerate([p for p in P if p < 1-nv]): 
                rates = [nv, mv, round(1 - (nv+mv),2)] 
                like = self.get_log_like(rates) 
                ld.append([like, rates]) 
            AD.append(sorted(ld)[0]) 
        scr, rates = sorted(AD)[0] 
        return rates  
            

    def get_log_like(self, rates): 
        log_like = 0 
        for p1,p2,p3 in self.probs: 
            log_like +=  -math.log((p1*rates[0]) + (p2*rates[1]) + (p3*rates[2]))
        return log_like  





#############################################################################################
##################################  H2 ESIMATION   ##########################################
#############################################################################################


class ConditionalHeritability:
    def __init__(self, pairs):
        self.pairs = pairs
        self.keys = sorted(self.pairs.keys())
        self.h_range = [round(0.0 + (i*0.01), 2) for i in range(101)]

    def estimate(self):
        self.PW = dd(lambda: dd(list))
        self.set_ptwise()
        self.run_ptwise('All')
        self.run_ptwise('Bod', A=4, B=95)
        self.run_ptwise('Mid', A=35, B=65)
        self.run_ptwise('LoH', A=5, B=40)
        self.run_ptwise('HiH', A=60, B=95)
        self.run_ptwise('LoT', A=-1, B=3)
        self.run_ptwise('HiT', A=96, B=101)
        return self.PW

    def set_ptwise(self):
        my_keys = sorted(self.pairs.keys())
        self.h_key = {h: self.set_log_like(h) for h in self.h_range}
        self.estimate1 = sorted(
            [[sum([sum(self.h_key[h][k]) for k in my_keys]), h] for h in self.h_key])[0][1]
        for hn in [round(self.estimate1 + (i+1.0) / 200, 3) for i in range(-60, 60, 2)]:
            if hn > 0.01 and hn < 0.99:
                self.h_key[hn] = self.set_log_like(hn)
        return

    def set_log_like(self, h):
        h_likes = dd(list)
        h_var = (1 - (h*h) / 4.0)
        h_frac = 1 / ((h_var ** 0.5) * ((2*math.pi)**0.5))
        for k in self.pairs.keys():
            for b, a in self.pairs[k]:
                Ne = ((b - ((h*a)/2.0)) ** 2) / (2*h_var)
                LP = math.exp(-Ne) * h_frac
                h_likes[k].append(-math.log(LP))
        return h_likes

    def run_ptwise(self, name='All', A=-1, B=101, iterations=50):

        my_keys = sorted([k for k in self.pairs.keys() if k >= A and k <= B])

        my_size = str(int(sum([len(self.pairs[k]) for k in my_keys])))

        my_estimate = sorted(
            [[sum([sum(self.h_key[h][k]) for k in my_keys]), h] for h in self.h_key])[0][1]
        my_obs, my_lists, my_scores = [], dd(list), dd(list)

        for h in self.h_key.keys():
            for k in my_keys:
                my_scores[h].extend(self.h_key[h][k])
                my_lists[h].append(self.h_key[h][k])

        my_lens = [len(sL) for sL in my_lists[h]]
        if min(my_lens) > 40:
            for itr in range(iterations):
                my_idxs = [sorted(random.sample(range(len(sL)), int(
                    len(sL)/3.0) + 1)) for sL in my_lists[h]]
                my_obs.append(sorted([[sum([sum([L[j] for j in idxs]) for L, idxs in zip(
                    SL, my_idxs)]), k] for k, SL in my_lists.items()])[0][1])
            my_mean, my_std, my_var = round(
                np.mean(my_obs), 3),  np.std(my_obs), np.var(my_obs)
            self.PW[name] = [my_size, my_estimate, my_mean, round(my_std, 3)]

        elif sum(my_lens) > 1000:
            idxs = range(len(my_scores[h]))
            self.subset_size = int(len(idxs)/3.0)
            for itr in range(iterations):
                random_indexes = sorted(random.sample(idxs, self.subset_size))
                my_obs.append(sorted(
                    [[sum([L[ri] for ri in random_indexes]), k] for k, L in my_scores.items()])[0][1])
            my_mean, my_std, my_var = round(
                np.mean(my_obs), 3),  np.std(my_obs), np.var(my_obs)
            self.PW[name] = [my_size, my_estimate, my_mean, round(my_std, 3)]

        elif my_estimate < 0.01:
            self.PW[name] = [my_size, 0.01, 0.01, 0.01]
        else:
            self.PW[name] = [my_size, my_estimate, my_estimate, 0.5]

        return


#############################################################################################
##################################       MAIN      ##########################################
#############################################################################################


def run_script(filename, options):

    #if options.out == 'out': 
    #    options.out = filename.split('/')[-1].split('-')[0]
    
    
    if options.out == 'NA':
        options.out = filename.split('/')[-1].split('-')[0]
    
    
    
    pltname = options.out.split('.')[0]
    fd = FamData(filename)

    h2_res = pd.DataFrame(data={'data': [], 'size': [], 'type': [], 'init': [], 'iter': [], 'sd': []})
    tail_res = pd.DataFrame(data={'data': [], 'size': [],  'indexsib': [], 'test': [], 'tail': [],'obs': [], 'exp': [], 'nrate': [],'mrate': [],'prate': [],'tstat': [], 'pv': []})
    data_res = pd.DataFrame(data={'data': [], 'ptiles': [], 'sib1': [], 'sib2': [], 'se': []})
    for k, p in fd.pairs.items():
        
        ptiles = sorted(p.keys())
        S1,S2 = [], [] 
        

        for pt in ptiles: 
            S1 = [DZ[0] for DZ in p[pt]]
            S2 = [DZ[1] for DZ in p[pt]]
            m1, m2, sm  = np.mean(S1), np.mean(S2), stats.sem(S2) 
            cur_res = pd.DataFrame(data={'data': k, 'ptiles': pt, 'sib1':m1, 'sib2': m2, 'se': sm},index=[0])
            data_res = pd.concat([data_res, cur_res], ignore_index=True)

        h2 = ConditionalHeritability(p).estimate()
        init, hList, tList = options.out+':'+k, [], []
        for h in ['All', 'Bod', 'Mid', 'LoH', 'HiH', 'LoT', 'HiT']:
            cur_res = pd.DataFrame(data={'data': k, 'size': h2[h][0], 'type': h, 'init': h2[h][1], 'iter': h2[h][2], 'sd': h2[h][3]}, index=[0])
            h2_res = pd.concat([h2_res, cur_res], ignore_index=True)

        
        kl = []
        tt = TailTests(k, p, h2).calculate()
        for loc in ['0', '0-1', '0-2', '0-3', '99', '98-99', '97-99', '96-99']:
            kd, kn = [], loc.split('-')[-1]
            for name, T in zip(['nv', 'md','ks'], [tt[loc]['NOVO'], tt[loc]['MEND'], tt[loc]['DIST']]):
                cur_res = pd.DataFrame(data={'data':k,'size':T.size,'indexsib': T.index_sib,'test':name,'tail':loc,
                    'obs': T.obs,'exp':T.exp,'nrate': T.n_rate, 'mrate': T.m_rate, 'prate': T.p_rate,'tstat': T.Z, 'pv': T.pv}, index=[0])
                tail_res = pd.concat([tail_res, cur_res], ignore_index=True)

        if options.plot:
            tP = TraitPlot(pltname, k).draw_summary_table(p, h2, tt)

    data_res.to_csv(options.out + ".data", index=False)
    h2_res.to_csv(options.out + ".h2", index=False)
    tail_res.to_csv(options.out + ".tail", index=False)


if __name__ == '__main__':
    from optparse import OptionParser
    usage = "usage: ./%prog [options] data_file"
    parser = OptionParser(usage=usage)
    parser.add_option("--plot", action='store_true', default=False,
                      help="Whether we should generate the plots")
    parser.add_option("--out", default='out', type='string',
                      help="Output file prefix")
    (options, args) = parser.parse_args()
    run_script(args[0], options)






