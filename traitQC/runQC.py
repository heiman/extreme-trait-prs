#!/usr/bin/python3
import sys
sys.path.append("/home/tade/Current/Tails/analysis/code/python_run2") 
from src.ExtremeLoad import traitLoad 


from scipy import stats 
from collections import defaultdict as dd
import numpy as np
import math
#from scipy import stats
from math import log

class Trait: 
    def __init__(self, traitID, K): 
        self.id = traitID 
        try: self.ti = int(self.id) 
        except: self.ti = self.id 
        for k in ['samples', 'h2', 'maxsnp', 'vals', 'val50', 'skew']: 
            if k in K: vars(self)[k] = K[k] 
            else:      vars(self)[k] = 'NA' 

        self.corr_pairs = []  




class MyTraits: 
    def __init__(self, options): 
        self.options = options 
        
    
    def read(self, traitFile): 
        self.members = []   
        with open(traitFile) as F: 
            header = F.readline().split() 
            if len(header) == 1: header = header[0].split(',') 
            for line in F:  
                line = line.split() 
                if len(line) == 1: line = line[0].split(',') 
                K = {}  
                for i in range(1,len(line)): 
                    try: K[header[i]] = float(line[i]) 
                    except: pass 
                self.members.append(Trait(line[0], K)) 
        return self  


    def filter(self, min_size, min_h2, min_vals, min_top_vals, max_snp, max_skew): 
        
        sys.stderr.write('QC Filtering '+str(len(self.members))+' Traits...') 
        dz = int(float(len(self.members))/20.0)
        
        self.key = {} 

        for i,m in enumerate(self.members): 
            if i % dz == 0: sys.stderr.write('.') 
            if m.samples != 'NA' and m.samples < min_size: continue 
            if m.h2 != 'NA' and m.h2 < min_h2: continue 
            if m.vals != 'NA' and m.vals < min_vals: continue 
            if m.val50 != 'NA' and m.val50 < min_top_vals: continue 
            if m.maxsnp != 'NA' and m.maxsnp > max_snp: continue 
            if m.skew != 'NA' and m.skew > max_skew: continue 
            self.key[m.ti] = m 
        
        sys.stderr.write('Finished ('+str(len(self.key))+' traits remain)\n') 
        

    def uniquify(self, corrFile, max_corr, out_file): 

        with open(corrFile) as F: 
            F.readline() 
            for line in F: 
                line = line.split() 
                if len(line) == 1: line = line[0].split() 
                t1, t2, rg = int(line[0]), int(line[1]), math.fabs(float(line[2])) 
                if t1 in self.key and t2 in self.key: 
                    if rg > max_corr: 
                        self.key[t1].corr_pairs.append([rg,t2]) 
                        self.key[t2].corr_pairs.append([rg,t1]) 
        

        sys.stderr.write('QC Unquifying '+str(len(self.key))+' Traits...') 

        self.REMOVE = dd(bool) 
        self.pass_key = {} 

        dz = int(float(len(self.key))/20.0)
        for j,(ti,T) in enumerate(self.key.items()): 
            if j % dz == 0: sys.stderr.write('.') 
            if self.REMOVE[ti]: continue 
            if len(T.corr_pairs) == 0: self.pass_key[ti] = T 
            else: 
                cp = [(r,tx) for (r,tx) in T.corr_pairs if not self.REMOVE[tx]] 
                if len(cp) == 0: self.pass_key[ti] = T 
                else: 
                    pair_samples = [self.key[tx].samples for r,tx in cp] 
                    if T.samples > max(pair_samples): 
                        self.pass_key[ti] = T 
                        for r,tx in cp: self.REMOVE[tx] = True  
                    else: continue 
        
        sys.stderr.write('Finished ('+str(len(self.pass_key))+' traits remain)\n') 

        
        w = open(out_file, 'w') 
        sys.stderr.write('Writing Traits to File ('+str(out_file)+')...') 

        dz = int(float(len(self.pass_key))/20.0)
        for j,ti in enumerate(self.pass_key.keys()): 
            if j % dz == 0: sys.stderr.write('.') 
            w.write('%s\n' % ti) 
        sys.stderr.write('Finished\n') 
















        
        sys.exit() 


    def qc(self): 
        P1, P2 = [], [] 
        self.RX = dd(list) 
        for t,T in self.key.items():
            fails = [] 
            #if T.ti in [20127]: fails.append('uniq50') 
            if T.h2 < self.options.minH2: fails.append('lowH2') 
            if T.rawCnt < self.options.minSize: fails.append('lowCnt') 
            if T.uniq50 < 3: fails.append('uniq50') 
            if len(fails) > 0: self.RX[t].append('Filter1:'+','.join(fails)) 
            else:              P1.append(T.ti) 
            fails = [] 
            
            if T.ti in [30080]: fails.append('skew') 
            if T.skew > 2 or T.skew < -2: fails.append('skew') 
            if T.maxSnp > 0.02:           fails.append('maxSnp') 
            if len(fails) > 0:            self.RX[t].append('Filter2:'+','.join(fails)) 
            else:                         P2.append(T.ti) 


       
        sys.stderr.write('  Filter1: '+'RawCnt>'+str(self.options.minSize)+' & h2>'+str(self.options.minH2)+' & uniq50>2\n') 
        sys.stderr.write('  After Filter1: '+str(len(P1))+' traits remain\n')     
            
        sys.stderr.write('  Filter2: abs_skew < 2, maxSnp < 0.02\n') 
        sys.stderr.write('  After Filter2: '+str(len(P2))+' traits remain\n')     
        keep, self.remove = {}, {} 
        for t,T in self.key.items(): 
            if T.ti not in P2: self.remove[T.ti] = T 
            else: keep[T.ti] = T 
        self.key = keep 





def run_script(traitData, corrData, options): 
    
    traits = MyTraits(options).read(traitData)  
   

    traits.filter(options.min_size, options.min_h2, options.min_vals, options.min_top_vals, options.max_snp, options.max_skew)  
    traits.uniquify(corrData, options.max_corr, options.outFile) 
    








if __name__ == '__main__':
    from optparse import OptionParser
    usage = "usage: ./%prog [options] data_file"
    parser = OptionParser(usage=usage)
    parser.add_option("--traitData", default = None, type=str, help="Raw File") 
    parser.add_option("--corrData", default = None, type=str, help="Raw File") 
    parser.add_option("--outFile", default = 'validTraits.txt', type=str, help="Raw File") 
    parser.add_option("--min_size", default = 100000, type=int, help="R2") 
    parser.add_option("--min_h2", default = 0.05, type=float, help="R2") 
    parser.add_option("--min_vals", default = 10, type=float, help="R2") 
    parser.add_option("--min_top_vals", default = 3, type=float, help="R2") 
    parser.add_option("--max_skew", default = 2, type=float, help="R2") 
    parser.add_option("--max_snp", default = 0.01, type=float, help="R2") 
    parser.add_option("--max_corr", default = 0.95, type=float, help="R2") 
    (options, args) = parser.parse_args()
    run_script(options.traitData, options.corrData, options) 





